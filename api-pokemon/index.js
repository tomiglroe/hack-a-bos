'use strict';

const pokemonRouter = require('./pokemon-router');
const proxyRouter = require('./proxy-router');
const testRouter = require('./tests-router');

module.exports = {
  pokemonRouter,
  proxyRouter,
  testRouter,
};
